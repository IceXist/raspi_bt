# README #

This is a program that detects Bluetooth signal and then unlocks a door using a Raspberry Pi.

Documentation is here: [PDF Document](https://bitbucket.org/IceXist/raspi_bt/raw/b75dc9466dfec3c9aed550859435a23a53962a04/Documentation/RaspiBT_doc.pdf)

### To Do ###
* Set Up Webserver
* Webpage to have bluetooth addresses set remotely
* ~~Finish Documentation~~
* ~~LED feedback~~