close all
clear all
clc

% ------


% time = [0 0 0 0 1 .05 1 .05 1 .05 1 .05 1 .05];
% time = [ 0  0 .5 .5...
%     .5 .5...
%     .5 .5 ...
%     .5 .5 ...
%     .5 .5];
% time = [0 0 .05 .05 .05 .05 1 .05...
%     .05 .05 .05 .05 1 .05...
%     .05 .05 .05 .05 1 .05...
%     .05 .05 .05 .05 1 .05...
%     .05 .05 .05 .05 1 .05];
time = [0 0 .05 .05...
     .05 .05...
      .05 .05 ];
height = .65;

timeshift = [0 0];
for j = 3:length(time)
    timeshift(j) = timeshift(j-1) + time(j-1);
end

fig = figure(1);
for i = 1:2:length(time)
    rectangle('Position',[timeshift(i),0,time(i),height],'facecolor','red');
end
daspect([1 1 1])
axis([0 7 0 height])
xlabel('Time')
set(gca,'yticklabel',[])
% set(gca,'xticklabel',[ ])

print(fig,'-dpng','led_4')