import bluetooth
import time
import RPi.GPIO as io
import sys
from multiprocessing import Process

# 2/12/15 - Kyle Bahnsen

# Set up IO
io.setmode(io.BCM) # Sets pin numbering

# Set Pins
dps_pin = 24
door_pin = 18
override_pin = 25
led_pin = 17

io.setup(dps_pin, io.IN, pull_up_down=io.PUD_UP)  # Set dps_pin as IN with Pull Up Resistor
io.setup(door_pin, io.OUT)  # Set door_pin as OUT
io.setup(override_pin, io.IN, pull_up_down=io.PUD_UP)
io.setup(led_pin,io.OUT)  # initializes LED pin

# Set up Bluetooth Addresses from btAddresses.txt
with open("btAddresses.txt","r") as addresses:
    btAddr = [line.strip() for line in addresses if not line.startswith("#")]


def ledcontrol(flash):
    if flash == "scan":
        io.output(led_pin,True)
        time.sleep(1)
        io.output(led_pin,False)
        time.sleep(.05)

    elif flash == "on":
        io.output(led_pin,True)

    elif flash == "off":
        io.output(led_pin,False)

    elif flash == "hold":
        for cnt in range(0,5):
            io.output(led_pin,True)
            time.sleep(.5)
            io.output(led_pin,False)
            time.sleep(.5)

    elif flash == "unlock":
        for cnt in range(0,5):
            io.output(led_pin,True)
            time.sleep(.05)
            io.output(led_pin,False)
            time.sleep(.05)
            io.output(led_pin,True)
            time.sleep(.05)
            io.output(led_pin,False)
            time.sleep(.05)
            io.output(led_pin,True)
            time.sleep(1)
            io.output(led_pin,False)
            time.sleep(.05)

    elif flash == "found":
        for cnt in range(0,3):
            io.output(led_pin,True)
            time.sleep(.05)
            io.output(led_pin,False)
            time.sleep(.05)


# Lock Control
def doorState(state):
    if state == "unlock":               # When Unlock is desired
        print("Door Unlocked")               # tell console whats going on
        # LED: Solid on
        io.output(door_pin,True)            # Set door_pin output to high
        global doorBinary                   # create global doorBinary for use later
        doorBinary = 1                      # set to 1 to show that door is unlocked
        while not io.input(dps_pin):        # While door closed...
            btName = bluetooth.lookup_name(btAddr[x])   # Check for bluetooth address
            if not btName:                  # If no valid bluetooth address
                break                       # leave the loop, return to main search loop
            time.sleep(.5)                  # Wait time in seconds between checks for bluetooth address
    if state == "lock":                 # When Lock is desired
        print("Door Locked")                 # update console
        io.output(door_pin,False)           # sets output pin to low
        doorBinary = 0                      # set to 0 to show locked
    if state == "hold":                 # holds lock after bluetooth detected valid and dps alarmed
        print("Holding Lock")                # update console
        # LED: blink hold blink hold
        time.sleep(60)                      # set time to wait before locking door
        io.output(door_pin,False)           # lock door
        doorBinary = 0                      # set to 0 to show locked
        time.sleep(10)                      # set hold time in seconds before scanning again


def main():
    while True:                                 # repeat forever
        if not io.input(override_pin):     # If kill switch is held down
            io.cleanup()
            ledcontrol("off")
            sys.exit("Script will now exit")    # exit script
        else:
            for x in range(0,len(btAddr)):                # Repeat for every btaddr in list
                # LED: slow blinking
                p1 = Process(target=ledcontrol,args=("scan",))  # Create new process object
                p1.start()  # Start process object
                btName = bluetooth.lookup_name(btAddr[x]) # try and read bluetooth name.
                print("Scanning")                          # Update console
                p1.join()  # Rejoing process object


                # Determine whether door should be unlocked or not

                if btName:                          # If btName is valid - simple name recieved from device
                    print("Found Address")
                    p2 = Process(target=ledcontrol,args=("found",))
                    p2.start()
                    p2.join()
                    if not io.input(dps_pin):               # If DPS is alarmed
                        p3 = Process(target=ledcontrol,args=("hold",))
                        p3.start()
                        doorState("hold") # Hold door in current state
                        p3.join()
                    else:                               # If DPS is not alarmed
                        if doorBinary == 0:                 # If door is currently locked
                            p4 = Process(target=ledcontrol,args=("unlock",))
                            p4.start()
                            doorState("unlock")                 # Unlock the door
                            p4.join()
                        else:                               # If the door is currently unlocked
                            doorState("lock")                   # lock the door
                else:                               # If btName is not valid
                    doorState("lock")                   # lock the door
    return


if __name__ == "__main__":
    doorState("lock")                       # Preliminary lock, sets doorBinary to 0 and ensures locked door
    main()
