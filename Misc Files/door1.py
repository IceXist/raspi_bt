import bluetooth
import time
import RPi.GPIO as io

# Set up IO
io.setmode(io.BCM) # Sets pin numbering

# Set Pins
dps_pin = 24
door_pin = 18

io.setup(dps_pin, io.IN, pull_up_down=io.PUD_UP) # Set dps_pin as IN with Pull Up Resistor
io.setup(door_pin, io.OUT) # Set door_pin as OUT

# Set up Bluetooth Detection
btAddr = ['BC:F5:AC:9D:B7:0D','A0:F4:50:80:2F:0B','74:45:8A:D0:D7:29'] # List of valid addresses

# Lock Control
def doorState( state ):
    if state == "unlock": # When Unlock is desired
        io.output(door_pin,True) # Set door_pin output to high 
        global doorBinary # create global doorBinary for use later
        doorBinary = 1 # set to 1 to show that door is unlocked
    if state == "lock": 
        io.output(door_pin,False) # sets output pin to low
        doorBinary = 0
    if state == "hold": # holds lock after bluetooth detected valid and dps alarmed
        io.output(door_pin,False)
        doorBinary = 0
#        print 'Hold Lock'
        time.sleep(120) # set hold time in seconds
    return


# Script Actions Begin
doorState("lock") # Preliminary lock, sets doorBinary to 0 and insures locked door
while True: # repeat forever
    print doorBinary
    for x in range(0,2): 
        btName = bluetooth.lookup_name(btAddr[x])
        print 'looking up address'
        if btName:
            print 'found address'
            if doorBinary == 0 and not io.input(dps_pin):
#                print doorBinary
                doorState("unlock")
                print 'door unlocked'
#        else: #doorBinary == 1:
        if io.input(dps_pin) or not btName:
            print 'Door Alarmed'
            doorState("lock")
            print 'door locked'
        if io.input(dps_pin) and btName:
            doorState("hold")
#    if not btName:
#        doorState("lock")
    time.sleep(0.5)
        
