import bluetooth
import time
import RPi.GPIO as io
import sys

# Set up IO
io.setmode(io.BCM) # Sets pin numbering

# Set Pins
dps_pin = 24
door_pin = 18
override_pin = 25
led_pin = 17

io.setup(dps_pin, io.IN, pull_up_down=io.PUD_UP)  # Set dps_pin as IN with Pull Up Resistor
io.setup(door_pin, io.OUT)  # Set door_pin as OUT
io.setup(override_pin, io.IN, pull_up_down=io.PUD_UP)
io.setup(led_pin,io.OUT)  # initializes LED pin


def ledcontrol(flash):
    if flash == "scan":
        io.output(led_pin,True)
        time.sleep(1)
        io.output(led_pin,False)
        time.sleep(.05)

    elif flash == "on":
        io.output(led_pin,True)

    elif flash == "off":
        io.output(led_pin,False)

    elif flash == "hold":
        for cnt in range(0,5):
            io.output(led_pin,True)
            time.sleep(.5)
            io.output(led_pin,False)
            time.sleep(.5)

    elif flash == "unlock":
        for cnt in range(0,5):
            io.output(led_pin,True)
            time.sleep(.05)
            io.output(led_pin,False)
            time.sleep(.05)
            io.output(led_pin,True)
            time.sleep(.05)
            io.output(led_pin,False)
            time.sleep(.05)
            io.output(led_pin,True)
            time.sleep(1)
            io.output(led_pin,False)
            time.sleep(.05)

    elif flash == "found":
        for cnt in range(0,3):
            io.output(led_pin,True)
            time.sleep(.05)
            io.output(led_pin,False)
            time.sleep(.05)

if __name__ == "__main__":
    while True:
        input_var = input("Things plz: ")
        if input_var == "exit":
            io.cleanup()
            sys.exit("bye")
        else:
            ledcontrol(input_var)
