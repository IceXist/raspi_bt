import bluetooth
import time
import RPi.GPIO as io

# Set up IO
io.setmode(io.BCM)

dps_pin = 24
door_pin = 18

io.setup(dps_pin, io.IN, pull_up_down=io.PUD_UP)
io.setup(door_pin, io.OUT)
click = False

def timer( click ):
    print 'running Timer'
	global time_cnt 
	time_cnt = 0
	while click == True:
		time_cnt = time_cnt + 1
		print time_cnt
		time.sleep(60)
	return

def doorState( state ):
    print 'running doorState'
	if state == "unlock":
		io.output(door_pin,True)
		dpsCheck(True)
	if state == "lock":
		io.output(door_pin,False)
		dpsCheck(False)
	return

def dpsCheck( xbv ):
    print 'running dpsCheck'
	while xbv == True:
		if io.input(dps_pin):
			print("DPS ARMED")
			doorState("lock")
			xbv = False
		if not io.input(dps_pin):
			print("DPS safe")
		time.sleep(0.5)
	return

# Set up Bluetooth Detection
btAddr = 'BC:F5:AC:9D:B7:0D'
print 'bluetooth address saved'

while True:
	btName = bluetooth.lookup_name(btAddr)
    print 'looking up address'
	if btName:
		doorState("unlock")
		print 'found device'
		timer(True)
		while time_cnt < 7:
			if time_cnt == 5:
				doorState("lock")
				timer(False)
	else:
		print 'No Devices Found'
	time.sleep(1)
