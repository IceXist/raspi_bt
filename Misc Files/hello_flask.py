from flask import Flask
from flask.ext.basicauth import BasicAuth

app = Flask(__name__)

app.config['BASIC_AUTH_USERNAME'] = 'john'
app.config['BASIC_AUTH_PASSWORD'] = 'matrix'


basic_auth = BasicAuth(app)

@app.route("/")
@basic_auth.required

def hello():
	return "Hello Bob! This is on the Pi."

if __name__ == "__main__":
	app.run(host='172.20.19.177', port=324, debug=True)
